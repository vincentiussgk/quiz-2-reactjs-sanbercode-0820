/*Asumsikan yang diminta soal berupa 1 fungsi volume
dimana fungsi bisa menghitung volume kubus atau volume balok
tergantung jumlah parameter.*/

const volume = (...arg) => {
	if(arg.length==3){
		let x = arg[0]*arg[1]*arg[2];
		return(`${x}`);
	}
	else{
		let x = arg*arg*arg;
		return(`${x}`);
	}
}
console.log(volume(5));
console.log(volume(5,2,10));
