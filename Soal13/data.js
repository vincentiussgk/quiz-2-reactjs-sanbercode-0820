import React, {Component} from 'react';

const data = [{name: "John", age: 25, gender: "Male", profession: "Engineer", photo: "https://media.istockphoto.com/photos/portarit-of-a-handsome-older-man-sitting-on-a-sofa-picture-id1210237745"}, 
{name: "Sarah", age: 22, gender: "Female", profession: "Designer", photo: "https://cdn.pixabay.com/photo/2018/01/15/07/51/woman-3083378_960_720.jpg"}, 
{name: "David", age: 30, gender: "Male", profession: "Programmer", photo: "https://media.istockphoto.com/photos/handsome-mexican-hipster-man-sending-email-with-laptop-picture-id1182472756"}, 
{name: "Kate", age: 27, gender: "Female", profession: "Model", photo: "https://cdn.pixabay.com/photo/2015/05/17/20/07/fashion-771505_960_720.jpg" }]

class Data extends React.Component{
    render(){
        return (
            <>
            <div>
                <div style={{display:"flex"}}>
                    <div style={{padding:"15px"}}>
                        <div>

                        
                        {
                            data.map(el=> {
                                return(
                                    <div style={{border:"1px solid black",borderRadius:"5px", margin:"5px"}}>
                                        <div>
                                            <img src={el.photo} style={{width:"475px",height:"300px"}}></img>
                                        </div><br></br>                                         
                                        <div><b>{el.gender=="Male"?"Mr":"Mrs"} {el.name}</b></div><br></br>
                                        <div>{el.age} years old</div><br></br>   
                                        <div>{el.profession}</div>                                 
                                    </div>
                                )
                            })                
                        }
                        </div>
                    </div>
                </div>
            </div>
            </>
        );
    }
}

export default Data;

